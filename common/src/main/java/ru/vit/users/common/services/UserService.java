package ru.vit.users.common.services;

import ru.vit.users.common.models.User;

import java.util.List;

public interface UserService {
    List<User> getUsers();
}
