package dao;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.Base64Utils;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;
import java.util.List;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:contexts/app-context.xml", "classpath:contexts/mvc-context.xml"})
@WebAppConfiguration(value = "src/main/web")

public class MainTest {
    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;
    @Before
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.
                webAppContextSetup(this.wac)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void mainControllerCreated() {
        ServletContext servletContext = wac.getServletContext();
        Assert.assertNotNull(servletContext);
        Assert.assertTrue(servletContext instanceof MockServletContext);
        Assert.assertNotNull(wac.getBean("mainController"));
    }

    @Test
    public void indexPage_IsUsers_And_Model_ContainsUsers() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk()).andReturn();
        Assert.assertTrue(((List)mvcResult.getModelAndView().getModel().get("users")).size()>0);
        Assert.assertTrue(mvcResult.getModelAndView().getViewName().equals("users"));
    }

    @Test
    public void security() throws Exception {
        mockMvc.perform(get("/hello")).andExpect(status().is(401)).andReturn();
        mockMvc.perform(get("/hello")
                .header(HttpHeaders.AUTHORIZATION, "Basic " + Base64Utils.encodeToString("user1:user1".getBytes())))
                .andExpect(status().isOk());
    }

}