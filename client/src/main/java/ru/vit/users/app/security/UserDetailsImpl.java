package ru.vit.users.app.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.vit.users.common.models.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class UserDetailsImpl implements UserDetails {
    private User user;
    private String password;
    private List<GrantedAuthority> roles = new ArrayList<>();

    public UserDetailsImpl(User user, List<String> roles, String password) {
        if (roles.size() == 0) roles.add("ROLE_REGISTERED");
        this.user = user;
        this.password = password;
        for (String role : roles) {
            this.roles.add(new SimpleGrantedAuthority(role));
        }
    }

    public UserDetailsImpl() {
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return user.getLogin();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
