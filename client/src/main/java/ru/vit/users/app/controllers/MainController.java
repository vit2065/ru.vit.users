package ru.vit.users.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.vit.users.common.models.User;
import ru.vit.users.services.UserServiceImpl;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class MainController {
    private UserServiceImpl userService;

    @Autowired
    public MainController(UserServiceImpl userServiceImpl) {
        this.userService = userServiceImpl;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(ModelMap model) {
        return users(model);
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String users(ModelMap model) {
        List<User> users = userService.getUsers();
        model.addAttribute("users", users);
        return "users";
    }

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String hello(ModelMap model, HttpSession session) {
        model.addAttribute("username", session.getAttribute("username"));
        return "hello";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
        public String login(ModelMap model, @RequestParam(required = false) String username) {
        model.addAttribute("username", username);
        return "login";
    }

    @RequestMapping(value = "/loginFailed", method = RequestMethod.GET)
    public String loginFailed(ModelMap model,
                              @RequestParam(required = false) String error,
                              @RequestParam(required = false) String username) {
        model.addAttribute("error", error);
        model.addAttribute("username", username);
        return "loginFailed";
    }
}
