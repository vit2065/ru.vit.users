package ru.vit.users.app;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@Configuration
@PropertySources({
        @PropertySource("classpath:queries.conf")
})
@ComponentScan(basePackages = {"ru.vit.users"})
public class MainConf {
}
