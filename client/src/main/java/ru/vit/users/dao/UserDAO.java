package ru.vit.users.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;
import ru.vit.users.app.security.UserDetailsImpl;
import ru.vit.users.common.models.User;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserDAO {
    JdbcTemplate jdbcTemplate;

    @Value("${query.users}")
    private String queryUsers;

    @Value("${query.userByLogin}")
    private String queryUserByLogin;

    @Autowired
    public UserDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<User> getUsers(){
        List<User> users = new ArrayList<>();
        RowCallbackHandler rs = resultSet -> users.add(new User(resultSet.getLong("id"), resultSet.getString("login")));
        jdbcTemplate.query(queryUsers, rs);
        return users;
    }

    public UserDetails getUserByLogin (String login) throws UsernameNotFoundException {
        List<UserWithPassword> users = new ArrayList<>();
        RowCallbackHandler rsh = rs -> users.add(new UserWithPassword(rs.getLong("id"), rs.getString("login"), rs.getString("password")));
        jdbcTemplate.query(queryUserByLogin, new Object[] { login }, rsh);
        if (users.size()==0) throw new UsernameNotFoundException(login);
        return new UserDetailsImpl(users.get(0), new ArrayList<>(), users.get(0).getPassword());
    }

    private class UserWithPassword extends User{
        private String password;

        UserWithPassword(Long id, String login, String password) {
            super(id, login);
            this.password = password;
        }

        String getPassword() {
            return password;
        }
    }
}
