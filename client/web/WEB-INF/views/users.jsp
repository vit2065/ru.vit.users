<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Список пользователей</title>
</head>

<body>
<h2>Список пользователей</h2>
<table>
    <tr>
        <th>id</th>
        <th>login</th>
    </tr>
    <c:forEach var="user" items="${users}">
        <tr>
            <td> <c:out value="${user.id}"/></td>
            <td> <a href="login?username=${user.login}"> <c:out value="${user.login}"/> </a></td>
        </tr>
    </c:forEach>
</table>
</body>
</html>